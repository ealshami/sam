from torch import nn
from mmseg.registry import MODELS
# from torch.hub import load_state_dict_from_url

@MODELS.register_module()
class SAMEncoder(nn.Module):
    def __init__(self, backbone, neck, head, pretrained = None) -> None:
        super().__init__()
        self.backbone_cfg = backbone
        self.neck_cfg = neck
        self.head_cfg = head
        self.backbone = MODELS.build(cfg=self.backbone_cfg)

    def forward(self, x):
        x = self.backbone(x)
        return x


