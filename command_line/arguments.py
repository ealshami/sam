import argparse
import utils

parser  =  argparse.ArgumentParser()

parser.add_argument('--img', default = None, help = "Path to an image")
parser.add_argument('--k', default = 5, type = int, help = "Number of clusters")
parser.add_argument('--cuda', action='store_true', help = "Use GPU")
