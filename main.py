import os
import models
import torch
from mmengine import Config
from mmseg.registry import MODELS
from utils import load_weights, visualize_clusters
from torchvision import transforms
from PIL import Image
from command_line.arguments import parser


if __name__ == "__main__":
    # making sure that the configuration file path is reachable when running from the command line
    cfg_path = os.path.join(os.path.dirname(__file__), "configs", "vit-base-p16_sam_headless.py")
    cfg = Config.fromfile(cfg_path)
    sam_encoder = MODELS.build(cfg.model)
    load_weights(sam_encoder, cfg.model.pretrained)
    sam_encoder.eval()

    args = parser.parse_args()


    img = Image.open(args.img).convert("RGB")
    input_image = transforms.ToTensor()(img)
    if args.cuda:
        sam_encoder = sam_encoder.to('cuda')
        input_image = input_image.to('cuda')
    with torch.no_grad():
        out = sam_encoder(input_image.unsqueeze(0))
        visualize_clusters(out[0].cpu(),  args.k, input_image.cpu())

    

    
        
