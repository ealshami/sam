from torch.hub import load_state_dict_from_url
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import numpy as np
import torch.nn.functional as F
import torch

def load_weights(model, url):
    # checking something
    checkpoint = load_state_dict_from_url(url, progress=True)
    state_dict = checkpoint['state_dict']
    model.load_state_dict(state_dict, strict=True)

# cluster the tensor with shape (B, C, H, W) channel-wise using K-means
def cluster(x, K):
    x = x.squeeze(0)
    C, H, W = x.shape
    x = x.reshape(C, H*W)
    x = x.permute(1, 0)
    kmeans = KMeans(n_clusters=K, random_state=0).fit(x)
    return kmeans.labels_.reshape(1, H, W)

def visualize_clusters(x, K, img):
    labels = cluster(x, K) # (1, H, W)
    labels = torch.from_numpy(labels)
    upscaled_labels = F.interpolate(labels.unsqueeze(0).float(), size=(img.shape[1], img.shape[2]), mode='bilinear', align_corners=False).squeeze(0).long()
    plt.imshow(img.permute(1, 2, 0))
    plt.imshow(upscaled_labels.permute(1, 2, 0), alpha=0.6)
    plt.axis('off')
    plt.savefig(f"demo_clusters_{str(K)}clusters.png", dpi=300)
    plt.show()