## Algorithm used is K-Means Clustering

## Usage

### Running the Script

To run the Python script, open the command prompt (cmd) and navigate to the root directory of the project.

#### Command:

```bash
python main.py --img demo.png --k 6
```
#### Arguments:
1. `--img`: The path to the image file.
2. `--k`: The number of clusters to be used in the image.
3. `--cuda`: Use this flag to enable CUDA support. (Optional)